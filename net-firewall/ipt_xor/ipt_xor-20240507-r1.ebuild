# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools linux-info linux-mod-r1

DESCRIPTION="iptables xor module"
HOMEPAGE="https://github.com/faicker/ipt_xor"
COMMIT="4cd47e0f31bec745d42fe0d54b980667bc6e8b57"
SRC_URI="https://github.com/faicker/ipt_xor/archive/${COMMIT}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	net-firewall/iptables
	virtual/linux-sources
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=(
	"${FILESDIR}"/key.patch
)

S="${WORKDIR}/ipt_xor-${COMMIT}"

src_configure() {
	set_arch_to_kernel
	default
}

src_compile() {
	emake -C userspace libxt_XOR.so
	local modlist=(
		xt_XOR=:kernel
	)
	local modargs=(
		KERNEL_DIR="${KV_OUT_DIR}"
	)
	linux-mod-r1_src_compile
}

src_install() {
	xtlibdir=$(pkg-config xtables --variable=xtlibdir)
	dodir "${xtlibdir}"
	insinto "${xtlibdir}"
	doins userspace/libxt_XOR.so
	linux-mod-r1_src_install
	insinto /usr/lib/modules-load.d
	newins "${FILESDIR}/modules-load.conf" "${PN}.conf"
}
