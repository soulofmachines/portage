# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools linux-info linux-mod-r1

DESCRIPTION="Iptables WireGuard obfuscation extension"
HOMEPAGE="https://github.com/infinet/xt_wgobfs"
COMMIT="29b279ba208ec59df6c9dc502d3868e65afbb474"
SRC_URI="https://github.com/infinet/xt_wgobfs/archive/${COMMIT}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	net-firewall/iptables
	virtual/linux-sources
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/xt_wgobfs-${COMMIT}"

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	set_arch_to_kernel
	default
}

src_compile() {
	emake libxt-local -C src
	mkdir "${S}/module"
	cp "${S}/src/Kbuild" "${S}/src/chacha.c" "${S}/src/chacha.h" "${S}/src/wg.h" "${S}/src/xt_WGOBFS.h" "${S}/src/xt_WGOBFS_main.c" "${FILESDIR}/Makefile" "${S}/module"
	local modlist=(
		xt_WGOBFS=:module
	)
	local modargs=(
		KDIR="${KV_OUT_DIR}"
	)
	linux-mod-r1_src_compile
}

src_install() {
	xtlibdir=$(pkg-config xtables --variable=xtlibdir)
	dodir "${xtlibdir}"
	emake libxt-install -C src DESTDIR="${ED}"
	linux-mod-r1_src_install
	insinto /usr/lib/modules-load.d
	newins "${FILESDIR}/modules-load.conf" "${PN}.conf"
}
