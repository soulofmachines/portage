# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="DPI bypass multi platform"
HOMEPAGE="https://github.com/bol-van/zapret"
SRC_URI="https://github.com/bol-van/zapret/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	net-libs/libnetfilter_queue
	net-firewall/iptables
	net-dns/bind
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/zapret-${PV}"

src_install() {
	dosbin mdig/mdig
	dosbin nfq/nfqws
	dosbin tpws/tpws
	newconfd "${FILESDIR}/nfqws.confd" nfqws
	newinitd "${FILESDIR}/nfqws.initd" nfqws
	exeinto /usr/share/zapret
	doexe blockcheck.sh
	insinto /usr/share/zapret
	doins -r common
	insinto /usr/share/zapret/files
	doins -r files/fake
	dosym ../../../sbin/nfqws /usr/share/zapret/nfq/nfqws
	dosym ../../../sbin/tpws /usr/share/zapret/tpws/tpws
	dosym ../../../sbin/mdig /usr/share/zapret/mdig/mdig
}
