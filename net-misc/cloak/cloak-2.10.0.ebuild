# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit go-module

DESCRIPTION="A censorship circumvention tool to evade detection by authoritarian state adversaries"
HOMEPAGE="https://github.com/cbeuw/Cloak"
SRC_URI="
	https://github.com/cbeuw/Cloak/archive/refs/tags/v${PV/_/-}.tar.gz -> ${P}.tar.gz
	${P}-vendor.tar.xz
"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/Cloak-${PV/_/-}"

src_compile() {
	ego build -ldflags "-X main.version=${PV}" ./cmd/ck-client
	ego build -ldflags "-X main.version=${PV}" ./cmd/ck-server
}

src_install() {
	dobin ck-client
	dobin ck-server
	newconfd ${FILESDIR}/ck-server.confd ck-server
	newconfd ${FILESDIR}/ck-client.confd ck-client
	newinitd ${FILESDIR}/ck-server.initd ck-server
	newinitd ${FILESDIR}/ck-client.initd ck-client
	insinto /etc/cloak
	newins example_config/ckclient.json ck-client.json
	newins example_config/ckserver.json ck-server.json
}
