# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES="
	autocfg@1.4.0
	bitflags@2.6.0
	cfg-if@1.0.0
	ctor@0.2.8
	equivalent@1.0.1
	hashbrown@0.15.0
	indexmap@2.6.0
	libc@0.2.159
	lock_api@0.4.12
	memchr@2.7.4
	parking_lot@0.12.3
	parking_lot_core@0.9.10
	proc-macro2@1.0.87
	quote@1.0.37
	redox_syscall@0.5.7
	scopeguard@1.2.0
	serde@1.0.210
	serde_derive@1.0.210
	serde_spanned@0.6.8
	smallvec@1.13.2
	syn@2.0.79
	toml@0.8.19
	toml_datetime@0.6.8
	toml_edit@0.22.22
	unicode-ident@1.0.13
	windows-targets@0.52.6
	windows_aarch64_gnullvm@0.52.6
	windows_aarch64_msvc@0.52.6
	windows_i686_gnu@0.52.6
	windows_i686_gnullvm@0.52.6
	windows_i686_msvc@0.52.6
	windows_x86_64_gnu@0.52.6
	windows_x86_64_gnullvm@0.52.6
	windows_x86_64_msvc@0.52.6
	winnow@0.6.20
"

inherit cargo

DESCRIPTION="Unlock vGPU functionality for consumer grade GPUs"
HOMEPAGE="https://github.com/mbilker/vgpu_unlock-rs"
SRC_URI="
	https://github.com/mbilker/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
	${CARGO_CRATE_URIS}
"

LICENSE=""
# Dependent crate licenses
LICENSE+=" MIT Unicode-DFS-2016"
SLOT="0"
KEYWORDS="~amd64"

src_install() {
	insinto /usr/$(get_libdir)
	doins $(cargo_target_dir)/libvgpu_unlock_rs.so

	exeinto /usr/bin
	doexe "${FILESDIR}"/nvidia-smi
	doexe "${FILESDIR}"/nvidia-vgpud
	doexe "${FILESDIR}"/nvidia-vgpu-mgr
}
